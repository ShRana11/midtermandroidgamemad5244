package com.example.sukhrana.androidmidtermgame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {
    private final String TAG = "SPARROW";

    // game thread variables
    private Thread gameThread = null;
    private volatile boolean gameIsRunning;

    // drawing variables
    private Canvas canvas;
    private Paint paintbrush;
    private SurfaceHolder holder;

    // Screen resolution varaibles
    private int screenWidth;
    private int screenHeight;
    String result = "playing...";

    // VISIBLE GAME PLAY AREA
    // These variables are set in the constructor
    int VISIBLE_LEFT;
    int VISIBLE_TOP;
    int VISIBLE_RIGHT;
    int VISIBLE_BOTTOM;

    // SPRITES
    Square bullet;
    int SQUARE_WIDTH = 100;

    Square enemy;

    Sprite player;
    Sprite sparrow;
    Bitmap CatImage;
    Bitmap CageImage;
    Point catPosition;
    Point  CagePosition;
    Rect CatHitBox;
    Rect CageHitBox;
    int sparrowx  ;
    int  sparrowy;

    Square bullets;


    // GAME STATS
    int score = 0;


    public GameEngine(Context context, int screenW, int screenH) {
        super(context);
        this.bullets = new Square(context,80, 1200, 40 );

        // intialize the drawing variables
        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        // set screen height and width
        this.screenWidth = screenW;
        this.screenHeight = screenH;
        this.sparrowx = 80;
        this.sparrowy = 1200;

        // setup visible game play area variables
        this.VISIBLE_LEFT = 20;
        this.VISIBLE_TOP = 20;
        this.VISIBLE_RIGHT = this.screenWidth - 20;
        this.VISIBLE_BOTTOM = (int) (this.screenHeight * 0.8);



        // initalize sprites
        this.player = new Sprite(this.getContext(), 80, 1200, R.drawable.player64);
        this.sparrow = new Sprite(this.getContext(),  450, 300, R.drawable.bird64);


        //cat setup
        this.CatImage = BitmapFactory.decodeResource(context.getResources(), R.drawable.cat64);

        // setup the initial position of player
        this.catPosition = new Point();
        this.catPosition.x = 850;
        this.catPosition.y = 1200;

        // setup the hitbox
        this.CatHitBox = new Rect(
                this.catPosition.x,
                this.catPosition.y,
                this.catPosition.x+ this.CatImage.getWidth(),
                this.catPosition.y + this.CatImage.getHeight());

        //Cage Setup
//        this.CageImage = BitmapFactory.decodeResource(context.getResources(), R.drawable.cat64);
//
        this.CagePosition = new Point();
        this.CagePosition.x = 920;
        this.CagePosition.y = 120;
       // setup the initial position of player
//
//        // setup the hitbox
        this.CageHitBox = new Rect(
                this.CagePosition.x,
                this.CagePosition.y,
                this.CagePosition.x+ 100,
                this.CagePosition.y + 100);


   }

    @Override
    public void run() {
        while (gameIsRunning == true) {
            updateGame();    // updating positions of stuff
            redrawSprites(); // drawing the stuff
            controlFPS();
        }
    }

    // Game Loop methods
    final int CageSpeed = 25;
    boolean catmovingRight = false;
    boolean cagemovingRight = false;
    boolean gameOver = false;
    boolean movingDown = false;
     int x = 0;
      int y = 0;
      int press = 0;
    int newX =0;
    int newY = 0;
    int collide = 0;

    public void updateGame() {
        int n = random();
        if (y == 0) {
            if (sparrow.getxPosition() + n >= 850 && sparrow.getyPosition() + n >= 1000) {
                this.sparrow.setxPosition(this.sparrow.getxPosition() - n);
                this.sparrow.setyPosition(this.sparrow.getyPosition() - n);
            } else if (sparrow.getxPosition() + n <= 850 && sparrow.getyPosition() + n >= 1000) {
                this.sparrow.setxPosition(this.sparrow.getxPosition() + n);
                this.sparrow.setyPosition(this.sparrow.getyPosition() - n);
            } else if (sparrow.getxPosition() + n >= 850 && sparrow.getyPosition() + n <= 1000) {
                this.sparrow.setxPosition(this.sparrow.getxPosition() - n);
                this.sparrow.setyPosition(this.sparrow.getyPosition() + n);
            } else {
                this.sparrow.setxPosition(this.sparrow.getxPosition() + n);
                this.sparrow.setyPosition(this.sparrow.getyPosition() + n);
            }

            //update hitbox
            // UPDATE HIS HITBOX

            this.sparrow.getHitbox().left = this.sparrow.getxPosition();
            this.sparrow.getHitbox().top = this.sparrow.getyPosition();
            this.sparrow.getHitbox().right = this.sparrow.getxPosition() + this.sparrow.getImage().getWidth();
            this.sparrow.getHitbox().bottom = this.sparrow.getyPosition() + this.sparrow.getImage().getHeight();

if(press == 0) {
    double a = this.bullets.getxPosition() - this.sparrowx;
    double b = this.bullets.getyPosition() - this.sparrowy;

    // d = sqrt(a^2 + b^2)

    double d = Math.sqrt((a * a) + (b * b));

    Log.d(TAG, "Distance to cage: " + d);

    // 2. calculate xn and yn constants
    // (amount of x to move, amount of y to move)
    double xn = (a / d);
    double yn = (b / d);

    // 3. calculate new (x,y) coordinates
    newX = this.bullets.getxPosition() - (int) (xn * 15);
    newY = this.bullets.getyPosition() - (int) (yn * 15);
    this.bullets.setxPosition(newX);
    this.bullets.setyPosition(newY);
    bullets.updateHitbox();
    Log.d("DDDD", +newX + "," + newY + "," + xn + "," + yn + "," + sparrowx);
//            newX = this.bullets.getxPosition() + 30;
//            newY = this.bullets.getyPosition() -  30;
//            this.bullets.setxPosition(newX);
//            this.bullets.setyPosition(newY);
//            this.bullets.updateHitbox();

}
            // @TODO: Collision detection between player and enemy
         if(sparrowx+10 >= newX && sparrowx -10 <= newX && newX != 80){
                press = 1;



                int newX1 = this.bullets.getxPosition() + 20;
                int newY1 = this.bullets.getyPosition() - 20;
                this.bullets.setxPosition(newX1);
                this.bullets.setyPosition(newY1);
                this.bullets.updateHitbox();
             if (CageHitBox.intersect(this.bullets.hitbox)) {
                 Log.d(TAG, "COLLISION!!!!!");
                 this.result = "Awesome! ..Touched the Cage";
                 // this.CageHitBox.left = this.CageHitBox.left  ;
                 // this.CageHitBox.right = this.CageHitBox.right;

                 movingDown = true;
                 collide = 1;

             }else{
                 if(newY1 < 0 && collide ==0) {
                     this.result = "Aww! ..Missed";
                 }
             }

            }

            if (catPosition.x <= this.screenWidth / 3) {
                catmovingRight = true;
            }

            if (catmovingRight == true) {
                this.catPosition.x = this.catPosition.x + CageSpeed;
                // 2. move the hitbox
                this.CatHitBox.left = this.CatHitBox.left + CageSpeed;
                this.CatHitBox.right = this.CatHitBox.right + CageSpeed;
                if (catPosition.x >= 890) {
                    catmovingRight = false;
                }


            } else {

                this.catPosition.x = this.catPosition.x - CageSpeed;
                // 2. move the hitbox
                this.CatHitBox.left = this.CatHitBox.left - CageSpeed;
                this.CatHitBox.right = this.CatHitBox.right - CageSpeed;

            }

        }  // adjust cage movement
        if(movingDown == true ){
            if (this.CageHitBox.bottom  < 1400) {

                this.CagePosition.y = this.CagePosition.y + CageSpeed;
//                // 2. move the hitbox
               this.CageHitBox.top = this.CageHitBox.top + CageSpeed;
               this.CageHitBox.bottom = this.CageHitBox.bottom + CageSpeed;
//                if (CageHitBox.left >= 890) {
//                    cagemovingRight = false;
//                }
            }

            if(this.CageHitBox.top  >= 1400) {
                movingDown = false;
                if (CageHitBox.intersect(this.CatHitBox)) {
                    Log.d(TAG, "COLLISION!!!!!");
                    this.result = "Wow! ..You Win";
                    x = 1;
                    y = 1;

                }
                else{
                    this.result = "Alas! ..You Loss";
                    x = 1;
                    y = 1;
                }


            }

        }if(x == 0) {

            if (CageHitBox.left <= 20) {
                cagemovingRight = true;
            }

            if (cagemovingRight == true) {
                //this.CagePosition.x = this.CagePosition.x + CageSpeed;
                // 2. move the hitbox
                this.CageHitBox.left = this.CageHitBox.left + CageSpeed;
                this.CageHitBox.right = this.CageHitBox.right + CageSpeed;
                if (CageHitBox.left >= 890) {
                    cagemovingRight = false;
                }


            } else {
                // this.CagePosition.x = this.CagePosition.x - CageSpeed;
                // 2. move the hitbox
                this.CageHitBox.left = this.CageHitBox.left - CageSpeed;
                this.CageHitBox.right = this.CageHitBox.right - CageSpeed;

            }
        }


       // int numbery = rr.nextInt(800);


        // restart hitbox
//

       //this.sparrow.updateHitbox();



    }

    public int random() {
        Random rr = new Random();
        try {
            gameThread.sleep(100);
        }
        catch (InterruptedException e) {

        }
        int numberx = rr.nextInt(400);
        Log.d("NNNNN", "DEBUG: " + numberx);

        return  numberx;

    }


    public void outputVisibleArea() {
        Log.d(TAG, "DEBUG: The visible area of the screen is:");
        Log.d(TAG, "DEBUG: Maximum w,h = " + this.screenWidth +  "," + this.screenHeight);
        Log.d(TAG, "DEBUG: Visible w,h =" + VISIBLE_RIGHT + "," + VISIBLE_BOTTOM);
        Log.d(TAG, "-------------------------------------");
    }



    public void redrawSprites() {
        if (holder.getSurface().isValid()) {

            // initialize the canvas
            canvas = holder.lockCanvas();
            // --------------------------------

            // set the game's background color
            canvas.drawColor(Color.argb(255,255,255,255));

            // setup stroke style and width
            paintbrush.setStyle(Paint.Style.FILL);
            paintbrush.setStrokeWidth(8);

            // --------------------------------------------------------
            // draw boundaries of the visible space of app
            // --------------------------------------------------------
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setColor(Color.argb(255, 0, 128, 0));

            canvas.drawRect(VISIBLE_LEFT, VISIBLE_TOP, VISIBLE_RIGHT, VISIBLE_BOTTOM, paintbrush);
            this.outputVisibleArea();
            paintbrush.setColor(Color.BLACK);
            canvas.drawRect(
                    this.bullets.getxPosition(),
                    this.bullets.getyPosition(),
                    this.bullets.getxPosition() + this.bullets.getWidth(),
                    this.bullets.getyPosition() + this.bullets.getWidth(),
                    paintbrush
            );

            // --------------------------------------------------------
            // draw player and sparrow
            // --------------------------------------------------------

            // 1. player
            canvas.drawBitmap(this.player.getImage(), this.player.getxPosition(), this.player.getyPosition(), paintbrush);

            // 2. sparrow
            canvas.drawBitmap(this.sparrow.getImage(), this.sparrow.getxPosition(), this.sparrow.getyPosition(), paintbrush);
            // 3. cat
            canvas.drawBitmap(CatImage, this.catPosition.x, this.catPosition.y, paintbrush);

            //Cat hitbox
            canvas.drawRect(this.CatHitBox.left,
                    this.CatHitBox.top,
                    this.CatHitBox.right,
                    this.CatHitBox.bottom,
                    paintbrush
            );



            //@TODO: Draw the enemy

            canvas.drawRect(this.CageHitBox.left,
                    this.CageHitBox.top,
                    this.CageHitBox.right,
                    this.CageHitBox.bottom,
                    paintbrush
            );



            // --------------------------------------------------------
            // draw hitbox on player
            // --------------------------------------------------------
            Rect r = player.getHitbox();
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setColor(Color.BLUE);
            canvas.drawRect(r, paintbrush);


            // --------------------------------------------------------
            // draw hitbox on Sparrow

            Rect r1 = sparrow.getHitbox();
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setColor(Color.RED);
            canvas.drawRect(r1, paintbrush);

            // --------------------------------------------------------
            paintbrush.setTextSize(50);
            paintbrush.setStrokeWidth(5);
            String screenInfo = "Result: " + this.result;
            canvas.drawText(screenInfo, 20, 90, paintbrush);

            // --------------------------------
            holder.unlockCanvasAndPost(canvas);
        }

    }

    public void controlFPS() {
        try {
            gameThread.sleep(17);
        }
        catch (InterruptedException e) {

        }
    }


    // Deal with user input
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_UP:
                break;
            case MotionEvent.ACTION_DOWN:
                this.sparrowx = (int) event.getX();
                this.sparrowy = (int) event.getY();
                break;
        }
        return true;
    }

    // Game status - pause & resume
    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        }
        catch (InterruptedException e) {

        }
    }
    public void  resumeGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

}